# BLIND SQL INJECTION 빰빰빰 빠바바밤!!!! 

[![DoctorChoi](./4lab_DoctorChoi.png "Made by 4lab_DoctorChoi")](https://07001lab.tistory.com/)

> 난 그저 webhacking.kr 를 편하게 풀고 싶었을 뿐임  
> -최박사-  

> SQL 만큼 더러운 문법도 없다.  
> -익명-  

## 사용법  

```text
npm install -g typescript
git clone https://gitlab.com/DoctorChoi/blind-sql-injection-drchoi-script.git
cd blind-sql-injection-drchoi-script
src/index.ts    53 ~ 126 번째줄 수정
tsc
node ./out/index.js
```

## 패치노트  

### 1.0  

* 일단 만들었음 아무튼 만들었음  

### 1.1  

* 16진수 2진수 우회기능 추가  
* 로그 출력 살짝 바꿈  
* 길이 구문과 값 구문의 T/F 값이 다를경우 대응용 상수 추가  
* 길이만 알아내면 될때 사용하는 상수 추가  
* 알아내지 못하고 실패했을시에도 소요된 시간을 출력하도록 수정 (v 1.1.1)  
* 문자를 알아내지 못했을때 그대로 멈추지 않고 다음 문자로 넘어가도록 수정 (v1.1.2)

### 1.2

* sleep 기반 BLIND SQLI 날려야할때 사용하는 FLAG 추가  
* ~~버그 오지게 많으니깐 로그레벨 2 이상으로 해놓고 출력 긁어서 머리로 보정하는게 좋을듯~~  
* 버그 떄려잡음(v 1.2.1)  

### 1.3  

* order by 를 위해 TRUE 값을 찾을수 없으면 OK / 찾을수 있으면 FAIL 띄우는 기능 추가  
* POST 부분의 form 을 body 로 변경  
* 헤더에 Content-Type: application/x-www-form-urlencoded 를 자동으로 넣어주지는 않음  

### 1.4

* order by 기능 안돌아가는거 수정함  
* 쓰는 사람이 없다니 슬픕니다 ㅜㅜ  
* 출력을 전부 소문자/대문자로 바꾸는 기능 추가  

## 설정해야할 부분들  

```typescript
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
/*공격 구문 입력부분*/

const __URL: string = "";
//URL

const __HEADER: request.Headers = 
{
    "User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0"
};
//헤더

const __START: number = 0;
const __VALUE_START: number = 48;
const __FIN: number = 100;
//__START = 글자수 찾기 시작값
//__VALUE_START = 값 찾기 시작값 (기본값 48 = '0')
//__FIN = 글자수 찾기 종료값

const __LENGTH_A: string = "";
const __LENGTH_B: string = "";
const __VALUE_A: string = "";
const __VALUE_B: string = "";
const __VALUE_C: string = "";
//__LENGTH = 길이 알아내는 구문
//__VALUE = 값 알아내는 구문

// __LENGTH_A + i + __LENGTH_B
// __VALUE_A + i2 + __VALUE_B + fbypass(i) + __VALUE_C

const __TRUE: string = "";
const __FALSE: string = "";
//__TRUE = 길이 구문이 true 일때 출력되는 값
//__FALSE = 길이 구문이 false 일때 출력되는 값

const __TRUE_V: string = __TRUE;
const __FALSE_V: string = __FALSE;
// const __TRUE_V: string = "";
// const __FALSE_V: string = "";
//__TRUE_V 값 구문이 true 일때 출력되는 값
// __FALSE_V 값 구문이 false 일때 출력되는 값
// 적당히 주석 바꿔가면서 쓸것

const __CASE: number = 0;
// 0 = 그대로 출력 / 1 = 대문자로 출력 / 2 = 소문자로 출력
// 나머지는 0으로 인식함

const __SLEEP: number = 0;
//출력값이 존재하지 않아서 sleep() 를 사용할때 적은 초
//0 이하이면 작동하지 않음

const __BYPASS: number = 0;
// 1 = 2진수로 우회 2 = 16진수로 우회 나머지 = ascii 사용
// *** 자동으로 0x 나 0b 를 달아주지는 않음 주의 *** //

const __LOG_LEVEL: number = 2;
// 1 = true 인경우에만 출력 2 = 이상한 출력이 나온 경우도 출력 3 = 전부 출력 / 그 이외의 수를 집어넣으면 1로 인식함

const __USE_GET: boolean = true;
//true = get / false = post
const __COOKIE: boolean = false;
//tue = 쿠키에다가 SQLI 날려야 하는경우에 사용 false = 평상시
const __JUST_LENGTH: boolean = false;
//길이만 알아내면 될때 true
const __ORDER_BY: boolean = false;
//order by 절을 이용한 공격 (select 1 from guys_tbl where (1=1 and 1=1)) 같은거 집어넣을때 true
//true로 올리게 되면 __TRUE / __TRUE_V 의 문자열을 찾을수 없으면 OK 찾을수 있으면 FAIL

/*상수선언 끗*/
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
```
